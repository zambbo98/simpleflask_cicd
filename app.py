from flask import Flask, request


app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello Bob from zambbo!'
    


@app.route('/add', methods=['GET'])
def add():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    return f"{a+b}"

@app.route('/sub', methods=['GET'])
def sub():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    return f"{a-b}"

app.run(host='0.0.0.0', port=8145)