import unittest
import requests

class TestUnit(unittest.TestCase):
    
    def setUp(self):
        self.host_addr = 'http://13.209.43.4:8145'

    def test_index(self):
        res = requests.get(f"{self.host_addr}")
        hello_str = res.text
        
        self.assertEqual('Hello Bob from zambbo!', hello_str)

    def test_add(self):
        res = requests.get(f"{self.host_addr}/add?a=5&b=3")
        result = res.text
        
        self.assertEqual(result, '8')

    def test_sub(self):
        res = requests.get(f"{self.host_addr}/sub?a=5&b=3")
        result = res.text
        
        self.assertEqual(result, '2')    
    
if __name__ == '__main__':
    unittest.main()
